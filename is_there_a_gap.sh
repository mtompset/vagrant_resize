#!/bin/bash

CURRENT_SIZE=`sudo lsblk /dev/sda | grep sda1 | sed -e "s# [ ]*# #g" | cut -f4 -d' ' | sed -e "s#[a-zA-Z]##g"`
TOTAL_SIZE=`sudo lsblk /dev/sda | grep "sda " | sed -e "s# [ ]*# #g" | cut -f4 -d' ' | sed -e "s#[a-zA-Z]##g"`
if [ "$CURRENT_SIZE" != "$TOTAL_SIZE" ]; then
    echo "Run the fix_gap.sh script."
else
    echo "Resizing is complete."
fi
