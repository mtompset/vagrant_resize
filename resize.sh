#!/bin/bash

if [ -z $1 ]; then
    echo "Expecting a parameter! e.g. ubuntu/bionic64"
    exit 1
fi

if [ -z $2 ]; then
    echo "Expecting a size parameter in MB! e.g. 65536 (64GB)"
    exit 1
fi
NEW_SIZE=$2

# Determine the VM's ID
if [ -d "./.vagrant" ]; then
    find .vagrant/machines/ | grep \/id | cut -f3 -d\/ | grep "$1" &> /dev/null
    if [ $? -eq 1 ]; then
        echo "($1) is not a valid machine name."
        echo Possible valid names might be:
        find .vagrant/machines/ | grep \/id | cut -f3 -d\/
        exit 1
    fi
    if [ -d "./.vagrant/machines/$1" ]; then
        ID_DIR="./.vagrant/machines/$1/virtualbox"
    else
        VM_NAME=`ls .vagrant/machines/ | head 1 | cut -f1 -d\/ | tr -d '[[:space:]]'`
        ID_DIR="./.vagrant/machines/$VM_NAME/virtualbox"
    fi
    VM_ID=`cat $ID_DIR/id`
else
    # set up basic VM to resize
    VANILLA_BOX=$1
    vagrant init ${VANILLA_BOX}
    vagrant up
    vagrant halt
    VM_ID=`cat ./.vagrant/machines/default/virtualbox/id`
fi
[[ $DEBUG -eq 1 ]] && echo "VM ID: ${VM_ID}"

# Find the drive IDs
vboxmanage showvminfo ${VM_ID} | grep -E "(SCSI|IDE|SATA[ ]*Controller)" | grep UUID | cut -f3 -d\( | cut -f2 -d' ' | tr -d ')' > /tmp/drive_ids.txt

# Find the largest drive
MAX_SIZE=0
DRIVE_ID=""
[[ $DEBUG -eq 1 ]] && echo "Found these drives:"
[[ $DEBUG -eq 1 ]] && cat /tmp/drive_ids.txt
for i in `cat /tmp/drive_ids.txt`; do
    DRIVE=`echo $i | tr -d '[:space:]'`
    vboxmanage showmediuminfo "${DRIVE}" | grep Capacity | cut -f2 -d: | sed -e "s# [ ]*# #g" | cut -f2 -d' ' > /tmp/size.txt
    SIZE=`cat /tmp/size.txt`
    rm /tmp/size.txt
    [[ $DEBUG -eq 1 ]] && echo "DRIVE: $DRIVE"
    [[ $DEBUG -eq 1 ]] && echo "SIZE: $SIZE"
    if [ $MAX_SIZE -lt $SIZE ]; then
        MAX_SIZE=$SIZE
        DRIVE_ID="$DRIVE"
    fi
done
rm /tmp/drive_ids.txt

if [ $MAX_SIZE -gt $NEW_SIZE ]; then
    echo "This script is not intended to shrink a drive from (${MAX_SIZE}) to (${NEW_SIZE})."
    exit 1
fi

# Determine where it is.
[[ $DEBUG -eq 1 ]] && echo "Using Drive: $DRIVE_ID"
DRIVE_FILE=`vboxmanage showmediuminfo "$DRIVE_ID" | grep Location | sed -e "s# [ ]*# #g" | cut -f2- -d' '`
EXTENSION=${DRIVE_FILE: -4}
if [ ! "$EXTENSION" = "vmdk" ]; then
    echo "Not sure what to do."
    exit 1
fi

# Determine the VDI file name
NEW_FILE=${DRIVE_FILE/vmdk/vdi}

# In case older versions of this script left things lying around.
if [ -f "${NEW_FILE}" ]; then
    vboxmanage closemedium "${NEW_FILE}" --delete &> /dev/null
    if [ $? -ne 0 ]; then
        echo "Check your VirtualBox Virtual Media Manager for orphaned files."
    fi
fi

# Clone VMDK to VDI format
vboxmanage clonehd "${DRIVE_FILE}" "${NEW_FILE}" --format vdi

# Resize the VDI
vboxmanage modifymedium disk "${NEW_FILE}" --resize ${NEW_SIZE}

# Determine the port on which the drive is attached
PORT=`vboxmanage showvminfo "${VM_ID}" | grep -E "(SCSI|IDE|SATA[ ]*Controller)" | grep UUID | grep "${DRIVE_ID}" | cut -f2 -d'(' | cut -f1 -d,`
[[ $DEBUG -eq 1 ]] && echo "PORT: ${PORT}"

# Determine the controller on which the drive is attached
CONTROLLER=`vboxmanage showvminfo "${VM_ID}" | grep -E "(SCSI|IDE|SATA[ ]*Controller)" | grep "${DRIVE_ID}" | cut -f1 -d'(' | sed -e 's#[[:space:]]*$##'`
[[ $DEBUG -eq 1 ]] && echo "CONTROLLER: ${CONTROLLER}"

# attach nothing to the port
vboxmanage storageattach "${VM_ID}" --storagectl "${CONTROLLER}" --port ${PORT} --medium none

# remove the old hard drive files.
vboxmanage closemedium disk "${DRIVE_ID}" --delete

# convert the VDI file back to VMDK
vboxmanage clonehd "${NEW_FILE}" "${DRIVE_FILE}" --format vmdk

# remove the VDI file
vboxmanage closemedium "${NEW_FILE}" --delete &> /dev/null
if [ $? -ne 0 ]; then
    echo "Check your VirtualBox Virtual Media Manager for orphaned files."
fi

# attach the converted version
vboxmanage storageattach "${VM_ID}" --storagectl "${CONTROLLER}" --port ${PORT} --medium "${DRIVE_FILE}" --type hdd
