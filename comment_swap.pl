#!/usr/bin/perl -Tw

use strict;
use warnings;

my @lines = <STDIN>;
foreach my $line (@lines) {
    if ( $line =~ /swap/ && $line !~ /^#/ ) {
        $line = "#$line";
    }
    print $line;
}
