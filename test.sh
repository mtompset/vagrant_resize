#!/bin/bash

for box in `cat boxes.txt`;
do
    distribution=`echo $box | tr -d '[:space:]'`
    ./resize.sh $distribution 65536
    read -p "Press ctrl+c to stop" wait
    ./clean.sh
done
