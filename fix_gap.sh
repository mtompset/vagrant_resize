#!/bin/bash

if [ -z "$1" ]; then
    echo "Please enter a valid machine name."
    ls ./.vagrant/machines
    exit 1
fi

VM_NAME=$1
echo "Bringing up machine with gap."
vagrant up ${VM_NAME}
echo "Brutally fixing partitions."
vagrant ssh ${VM_NAME} -c "bash /vagrant/resize_now.sh"
echo "Rebooting..."
vagrant halt ${VM_NAME}
vagrant up ${VM_NAME}
echo "Resizing partition."
vagrant ssh ${VM_NAME} -c "sudo resize2fs /dev/sda1"
echo "Shutting down."
vagrant halt ${VM_NAME}
echo "Please recheck."
