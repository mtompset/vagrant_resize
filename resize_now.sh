#!/bin/bash

sudo swapoff --all
echo "RESUME=none" | sudo tee /etc/initramfs-tools/conf.d/resume

# create a back up, in the case that readonly-ness happens
GOOD_FSTAB=`sudo cat /etc/fstab | grep -v "^$" | wc -l`
if [ $GOOD_FSTAB -gt 1 ]; then
    sudo cp /etc/fstab /etc/fstab.before_resize
fi

sudo cat /etc/fstab | /vagrant/comment_swap.pl | sudo tee /etc/fstab
touch /tmp/fdisk_script
rm /tmp/fdisk_script
COUNT=`sudo fdisk -l /dev/sda | grep "^/dev/sda" | wc -l`
for i in `seq 1 $COUNT`
do
    echo d >> /tmp/fdisk_script
    if [ $i -ne $COUNT ]; then
        echo >> /tmp/fdisk_script
    fi
done
echo n >> /tmp/fdisk_script
echo p >> /tmp/fdisk_script
echo >> /tmp/fdisk_script
echo >> /tmp/fdisk_script
echo >> /tmp/fdisk_script
echo w >> /tmp/fdisk_script
echo "Built the instructions:"
cat /tmp/fdisk_script
sudo fdisk /dev/sda < /tmp/fdisk_script
echo "Repartitioned... need a reboot"
