#!/bin/bash

export `sudo blkid -o export /dev/sda1`
# <filesystem> <mount point> <type> <options> <dump> <pass>
sudo grep $UUID /etc/fstab
if [ $? -eq 0 ]; then
    echo "Are you sure you should be messing with /etc/fstab?"
    exit 1
fi
echo "UUID=$UUID / $TYPE errors=remount-ro 0 1" | sudo tee -a /etc/fstab
