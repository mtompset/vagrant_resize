#!/bin/bash

if [ -z "$1" ]; then
    echo "Please enter a valid machine name."
    ls ./.vagrant/machines
    exit 1
fi

VM_NAME=$1
echo "Bringing up machine with potential gap."
vagrant up ${VM_NAME} &> /dev/null
echo "Checking..."
vagrant ssh ${VM_NAME} -c "bash /vagrant/is_there_a_gap.sh" 2> /dev/null
vagrant halt ${VM_NAME} &> /dev/null
