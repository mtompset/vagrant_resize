# Vagrant Resize

This project addresses an annoyance I had: the default providers gave
us insufficiently sized vagrant boxes.  I am sure this could be adapted
for other virtualization methods, but this is intended for VirtualBox.

__DISCLAIMER__
Your mileage may vary. Please be aware I am not responsible for your
use of this and any resulting data loss that may or may not occur.

## Files

### boxes.txt

These are the boxes to be tested.  Basically each line represents what
you would use with the vagrant init command.

For example:
```
vagrant init ubuntu/bionic64
```
is represented by a line with just
```
ubuntu/bionic64
```
in this file.

There is generally no reason to edit this, unless you wish to...

### test.sh

This script runs through each line in the boxes.txt file and attempts
to get the official default box and then resize it.

It has a "Press ctrl+c to stop" prompt, but "read -p" seems to timeout,
and sometimes enter is pressed in a moment of impatience.

Debian distributions currently do not auto-magically resize to fill
the filesystem.  When the "Press ctrl+c to stop" is visible, in another
window you will need to run...

### fix_gap.sh

Before running this on the host, it might be an idea to vagrant up
and vagrant ssh into the guest to ensure that the filesystem is not
read only. If it is, there is likely nothing in the /etc/fstab file.
Check if there is a back up. The resize_now script purposefully copies
it, because this strangely happens a lot.

```
sudo ls /etc/fstab*
```
Tab completion does not work in the read only environment, so full names
and paths will have to be used to copy files into place.  Make sure the
swap lines are all commented out.

If there is no backup, check out the rebuild_empty_fstab.sh script
information in the sections below.

Once this is done, exit, vagrant halt and confirm the file system comes
up readable each time:
```
vagrant up; vagrant ssh
mount | grep sda1
exit
vagrant halt
```

__Only run this if the box is halted!__

This script should be used on boxes that do not automatically resize.
It depends on comment_swap.pl and resize_now.sh to run properly.  This is
quite ugly in that it turns off swap, deletes all the partitions, creates
just one, and they after a reboot resizes to fit the file system and
shuts down with a reminder to check.

This is a *very dangerous* script.

It takes one parameter, which is the name of a VM. If it is not
provided, the script displays an ls to show what could be valid.

### comment_swap.pl

Since perl is much easier to process strings on, this is used to comment
out swap entries in the /etc/fstab. It can be used to comment out anything
that has swap and isn't commented with an initial # symbol.

There is no need to run this directly. It runs on the guest.

### resize_now.sh

This does the ugly, partition-destroying processing.  There is no need to
run this directly, unless you wish to step through the resizing process,
because something does not quite work with a new distribution.

There is no need to run this directly. It runs on the guest.

### resize.sh

This is the star script. It takes two parameters.

The first represents the official vanilla box that is being resized
(e.g. ubuntu/bionic64), or the "name" of the machine
(i.e. "vagrant up stretch" would mean "stretch").

The second represents the size in megabytes (e.g. 65536 = 64GB).

```
./resize.sh debian/jessie64 32768
```
This runs the resizing logic on a default Debian Jessie box making
sure it is at least 32GB in size.  Currently, it seems to work without
issue on Ubuntu boxes, but not Debian.  Debian boxes require the more
forceful fix_gap.sh to be used as well.

```
./resize.sh default 65536
```
The "name" of the machine installed by the first command would be
"default". This resizes the existing box to 64GB.

If this script is copied and run in a different vagrant box directory,
it might be a different name (e.g. in a kohadevbox valid names include
stretch, bionic, etc.).

This script assumes the second format is being used if the .vagrant
directory exists. If this isn't the case, then run the clean.sh script
first. That will remove the .vagrant directory and the Vagrantfile,
allowing the first format to be used.

### clean.sh

This destroys the default VM, removes the Vagrantfile, and gets rid of
the .vagrant directory. It is as if you just freshly cloned this.

### rebuild_empty_fstab.sh

In the bizarre case your fstab goes missing, this can be used to fix
the fstab file.

```
sudo mount -o remount,rw /dev/sda1 /
/vagrant/rebuild_empty_fstab.sh
```
Then reboot, and hopefully all will be well enough. Other mountable
drives, like DVD/CD Drives, may need fixing as well manually.

### should_i_fix.sh

Debian vanilla boxes end up leaving a gap between the partition and the
end of the physically resized drive. This may be true of other vanilla
official vagrant boxes.

```
./should_i_fix.sh default
```
This should determine if there is a gap. It is run on the host.

__DISCLAIMER__
Fixing this gap is a dangerous process, any data loss is not
my responsibility.

The should_i_fix.sh script takes the machine name as a parameter, like
fix_gap.sh, and is slow because it brings the machine up and shuts
it down.

It uses the next script.

### is_there_a_gap.sh

When there is a gap, lsblk sizes for sda and sda1 differ. This script
determines the sizes, compares them, and then outputs an appropriate
recommendation.

There is no need to directly run this script. It runs on the guest.


## Purpose

Once the default box is resized, the intent was to push the resized box
back up to vagrant cloud, so it could be used, rather than official,
vanilla boxes.

To do this, an account on vagrant cloud (https://app.vagrantup.com/)
is needed.  Once the box is sized appropriately (or at least larger
currently), it would seem that:
```
vagrant cloud auth login

vagrant package --output meaningful_name.box

vagrant cloud publish your_userid/meaningful_name 1.0.0 virtualbox meaningful_name.box -d "A really cool box to download and use" --version-description "A short but meaningful description" --release --short-description "A short description"
```
will push it to the vagrant cloud. This part is still in discovery for me.
